# Manual person matching tool

## Project setup
```
npm install
```

### Data preparation

```
python scripts/prepare_data.py
python scripts/prepare_data.py -o buyer_seller_with_some_neighbors_candidate0  -i ../../extracted/buyer_seller_with_some_neighbors_candidate0 -f
python scripts/prepare_data.py -o buyer_seller_with_some_neighbors_candidate1  -i ../../extracted/buyer_seller_with_some_neighbors_candidate1 -f
python scripts/prepare_data.py -o buyer_seller_with_some_neighbors_candidate2  -i ../../extracted/buyer_seller_with_some_neighbors_candidate2 -f
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

