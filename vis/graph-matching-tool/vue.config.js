// vue.config.js

const CopyWebpackPlugin = require('copy-webpack-plugin');
var webpack = require('webpack')

module.exports = {
    publicPath: process.env.NODE_ENV === 'production'
        ? 'https://graphletmatchmaker.github.io/AVIZ-Tovanich-MC1/matchmaker/'
        : '/',
    chainWebpack: config => {
        config.module
            .rule('csv')
            .test(/\.csv$/)
            .use('csv-loader')
            .loader('csv-loader')
            .tap(options => {
                // modify the options...
                return {
                    dynamicTyping: true,
                    header: true,
                    skipEmptyLines: true
                }
            })
    }
}
