import pandas as pd
import os, shutil, errno
from pathlib import Path
import itertools
import numpy as np
from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import reverse_cuthill_mckee
import os
import datetime
import json

import time_distances

# This file is very similar to the one in ../person-pairing-view

DATA_FOLDER = "../../data/"
DUMP_FOLDER = "./public/data/"
DISTANCE_FOLDER = "../../distances/"

def add_distances(pairing, distances, distance_cols=None):
    if distance_cols is None:
        distance_cols = [c for c in distances.columns if
                         c not in ["templateID", "candidateID", "templateType", "candidateType", "graphName"]]
    df = pairing.copy()
    for c in distance_cols:
        df[c] = np.NaN
    for index, row in df.iterrows():
        scores = distances[
            (distances["templateID"] == row["templateID"]) & (distances["candidateID"] == row["candidateID"])]
        if len(scores.index) > 0:
            for c in distance_cols:
                df.loc[index, c] = scores.iloc[0][c]
        else:
            for c in distance_cols:
                df.loc[index, c] = np.NaN
    return df


def edge_status(edge, pairing, otherEdges, input_is_template):
    # Edge status is 'common' if an edge of same type exists on both sides
    # Not comparing location or time!
    e_col = "templateID" if input_is_template else "candidateID"
    match_col = "candidateID" if input_is_template else "templateID"

    matching_src = pairing[pairing[e_col] == edge["Source"]][match_col].values
    matching_tgt = pairing[pairing[e_col] == edge["Target"]][match_col].values

    if len(matching_src) == 0 or len(matching_tgt) == 0:
        return "out of pairing"

    matching_edges = otherEdges[(otherEdges["Source"] == matching_src[0]) & \
                                (otherEdges["Target"] == matching_tgt[0]) & \
                                (otherEdges["eType"] == edge["eType"])]
    if len(matching_edges.index) > 0:
        return "common"
    else:
        return "missing" if input_is_template else "supplementary"


def annotate_edges(pairing, c_edges, t_edges, t_status_col, c_status_col):
    c_edges[c_status_col] = c_edges.apply(
        lambda row: edge_status(row, pairing, t_edges, input_is_template=False), axis=1)
    t_edges[t_status_col] = t_edges.apply(
        lambda row: edge_status(row, pairing, c_edges, input_is_template=True), axis=1)
    return c_edges, t_edges


def get_spending_profile(nodes, edges, categories):
    edges = edges.merge(nodes, how="left", left_on="Source", right_on="NodeID").rename(
        columns={"NodeType": "SourceType"}).drop("NodeID", axis=1)
    edges = edges.merge(nodes, how="left", left_on="Target", right_on="NodeID").rename(
        columns={"NodeType": "TargetType"}).drop("NodeID", axis=1)

    spent = edges.loc[(edges['SourceType'] == 1) & (edges['TargetType'] == 4), ['Source', 'Target', 'Weight']]
    spent = spent.rename(columns={"Source": "Person", "Target": "NodeID", "Weight": "Amount"})
    spent['Amount'] = -1 * spent['Amount']

    received = edges.loc[(edges['SourceType'] == 4) & (edges['TargetType'] == 1), ['Source', 'Target', 'Weight']]
    received = received.rename(columns={"Target": "Person", "Source": "NodeID", "Weight": "Amount"})

    financial = pd.concat([spent, received], sort=False)
    financial = financial.groupby(['Person', 'NodeID']).agg({'Amount': 'sum'
                                                             }).reset_index()


    cartesian = (financial[['Person']].drop_duplicates().assign(key=1)
                 .merge(categories.assign(key=1), how='outer', on="key").drop("key", axis=1))

    financial = cartesian.merge(financial, how='left', on=['Person', 'NodeID'])
    return pd.pivot_table(financial, values='Amount', index=['Person'], columns=['NodeID'])


def squarify_edge_list(edges):
    edge_matrix = edges.groupby(by=["Source", "Target"]).agg(['count'])
    edge_matrix.columns = edge_matrix.columns.get_level_values(1)
    edge_matrix = edge_matrix.unstack(level=-1).fillna(0).astype(int)
    edge_matrix.columns = edge_matrix.columns.get_level_values(1)

    # Add missing columns
    missing_columns = set(edge_matrix.index) - set(edge_matrix.columns)
    for c in missing_columns:
        edge_matrix[c] = 0
    # Add missing rows
    missing_rows = set(edge_matrix.columns) - set(edge_matrix.index)
    for r in missing_rows:
        edge_matrix.loc[r] = [0] * len(edge_matrix.columns)
    return edge_matrix


def get_country(nodes, edges):
    person_ids = nodes[nodes["NodeType"] == 1]["NodeID"].unique()
    person_country = edges[edges["eType"] == 1][["Source", "SourceLocation"]] \
        .groupby(by="Source") \
        .first()
    person_country = person_country.rename(columns={"SourceLocation": "Country"})

    person_country_target = edges[edges["eType"] == 1][["Target", "TargetLocation"]] \
        .groupby(by="Target") \
        .first()

    person_country["Country"].fillna(person_country_target.TargetLocation, inplace=True)
    return person_country["Country"].fillna("")



def annotate_nodes(nodes, edges, categories):
    # Add spending profile
    category_dict = categories.set_index("NodeID")["Category"].to_dict()
    spending = get_spending_profile(nodes, edges, categories)
    spending = spending.rename(columns=category_dict)
    spending.columns = spending.columns.get_level_values(0)
    if len(spending.index) > 0:
        nodes = nodes.join(spending, on="NodeID", how="left")
        nodes["NodeID"] = nodes["NodeID"].astype(int)
    else:
        for c in spending.columns:
            nodes[c] = np.NaN

    # Add order minimizing crossings
    edges_ = edges[edges["eType"].isin([0, 1])][["Source", "Target", "Weight"]]
    edge_matrix = squarify_edge_list(edges_)
    index_order = edge_matrix.index
    edge_matrix = csr_matrix(edge_matrix.values)
    node_order = reverse_cuthill_mckee(edge_matrix)
    node_order = pd.Series(node_order, index=index_order, name="edgeOrder")

    # Add node without edges
    missing_rows = set(nodes["NodeID"].unique()) - set(node_order.index)
    for r in missing_rows:
        node_order.loc[r] = len(node_order.index)
    nodes = nodes.join(node_order.to_frame(), on="NodeID")

    # Profile: add person country and demographic flags
    node_countries = get_country(nodes, edges)
    nodes = nodes.join(node_countries.to_frame(), on="NodeID")

    if len(spending.index) > 0:
        nodes["F_Smoker"] = nodes["Tobacco"] > 0
        nodes["F_Education"] = nodes["Education"] > 0
        nodes["F_Gas"] = nodes["Natural gas"] > 0 if "Natural gas" in nodes else False
        nodes["F_Property"] = nodes["Property taxes"] > 0 if "Property taxes" in nodes else False
        nodes["F_Mortgage"] = nodes["Mortgage payments"] > 0 if "Mortgage payments" in nodes else False
        nodes["F_Renting"] = nodes["Rented dwellings"] > 0 if "Rented dwellings" in nodes else False
        nodes["F_Rich"] = nodes["Money income before taxes"] > 50000 if "Money income before taxes" in nodes else False
    else:
        for c in ["F_Smoker", "F_Education", "F_Gas", "F_Property", "F_Mortgage", "F_Renting", "F_Rich"]:
            nodes[c] = np.NaN

    return nodes

def normalize_timestamps(edges, is_template=False):
    # Normalize to large graph time, apply before grouping!
    origin = datetime.datetime(2025, 1, 1, 0, 0)
    if is_template:
        # Exactly 14 days shift
        edges["Time"] = edges["Time"].astype(int) + datetime.timedelta(days=14).total_seconds()
    # Processed format to unix timestamps
    if np.issubdtype(edges["Time"].dtype, np.number):
        # Time is in seconds from 12:00 AM Jan. 1, 2025 (like Unix timestamps, except that time=0 is the year 2025, not 1970)
        edges["Time"] = pd.to_datetime(edges["Time"], unit='s', origin=pd.to_datetime(origin)).values.astype(
            np.int64) // 10 ** 9
    else:
        # Time is formatted as string
        edges["Time"] = pd.to_datetime(edges["Time"], infer_datetime_format=True).values.astype(np.int64) // 10 ** 9

    #edges['EndTime'] = edges.apply(
    #    lambda row: int(row['Time'] + row['Weight']*24*60*60)
    #    if row['eType']==6 else None , axis=1)
    return edges


def normalize_metric_values(df):
    distance_cols = [c for c in df.columns if
                     c not in ["templateID", "candidateID", "graphName", "templateType", "candidateType"]]
    for c in distance_cols:
        if "Cosine" in c:  # Cosine similarity
            df.loc[:, c] = (1 - df[c]) / 2
        if "Jaccard" in c:  # Jaccard index
            df.loc[:, c] = 1 - df[c]
        if "dtw" in c:  # Time distance
            df.loc[:, c] = df[c] / df[c].max()
        if "gl" in c:  # Graphlet distance?
            df.loc[:, c] = df[c] / df[c].max()
    return df


def extract_subgraph_nodes(all_nodes, candidate_edges):
    candidate_node_ids = set(candidate_edges["Source"].unique()) | set(candidate_edges["Target"].unique())
    return all_nodes[all_nodes["NodeID"].isin(candidate_node_ids)]


def process_dataframes_candidate(template_edges, template_nodes, candidate_graphs, distances, categories, dump_folder=None):
    # Create output file if provided and necessary
    if dump_folder and not os.path.exists(dump_folder):
        os.makedirs(dump_folder)

    for candidate_name, candidate_graph in candidate_graphs.items():
        candidate_nodes = candidate_graph["nodes"]
        candidate_edges = candidate_graph["edges"]
        candidate_nodes = annotate_nodes(candidate_nodes, candidate_edges, categories)
        candidate_edges = normalize_timestamps(candidate_edges, is_template=False)

        if dump_folder:
            candidate_folder = dump_folder + "/" + candidate_name
            if not os.path.exists(candidate_folder):
                os.makedirs(candidate_folder)
            candidate_nodes.to_csv(candidate_folder + '/candidate_nodes.csv', index=False)
            candidate_edges.to_csv(candidate_folder + '/candidate_edges.csv', index=False)
            distances[candidate_name].to_csv(candidate_folder + '/distances.csv', index=False)

def prepare_pairing(graph_distances, candidate_name, candidate_nodes, candidate_edges, template_nodes,
                    template_edges, edge_filename=None):
    candidate_edges = flatten_edges(candidate_edges)
    candidate_edges, template_edges = annotate_edges(pairing, candidate_edges, template_edges, candidate_name)
    if edge_filename:
        candidate_edges.to_csv(edge_filename, index=False)
    return template_edges, candidate_edges, pairing


def prepare_data(template_edges, template_nodes, candidate_graphs, distances, categories, dump_folder=None):
    # Create output file if provided and necessary
    if dump_folder and not os.path.exists(dump_folder):
        os.makedirs(dump_folder)

    # Add spending profile, demographic properties and flags
    template_nodes = annotate_nodes(template_nodes, template_edges, categories)
    template_edges = flatten_edges(template_edges)

    candidates = {}
    for candidate_name, candidate_graph in candidate_graphs.items():
        candidate_nodes = candidate_graph["nodes"]
        candidate_edges = candidate_graph["edges"]
        candidate_nodes = annotate_nodes(candidate_nodes, candidate_edges, categories)
        candidate_edges = flatten_edges(candidate_edges)
        if dump_folder:
            candidate_edges.to_csv(dump_folder + '/candidate_edges_' + candidate_name + '.csv', index=False)
            candidate_nodes.to_csv(dump_folder + '/candidate_nodes_' + candidate_name + '.csv', index=False)
            distances[candidate_name].to_csv(dump_folder + '/candidate_distances_' + candidate_name + '.csv', index=False)
    if dump_folder:
        template_edges.to_csv(dump_folder + '/template_edges.csv', index=False)
        template_nodes.to_csv(dump_folder + '/template_nodes.csv', index=False)

def add_time_travel(distances, subgraph_name):
    travel_channel = pd.read_csv(
        DISTANCE_FOLDER + "/time_distances_travel_channel/" + "dtw_" + subgraph_name + ".csv")  # TemplateId,Id,Distance
    travel_channel = travel_channel.rename(
        columns={'TemplateId': 'templateID', 'Id': 'candidateID', 'Distance': 'dtwTravel'})
    travel_channel["graphName"] = subgraph_name
    distances = distances.merge(travel_channel, how="outer", on=["templateID", "candidateID"])
    return distances

def add_graphlet(distances, node_ids):
    gl0 = pd.read_csv(DISTANCE_FOLDER + "gl_0_undir5_similarity.csv")  # TemplateNode,PersonNode,graphletSimilarity
    gl0 = gl0.rename(columns={'TemplateNode': 'templateID', 'PersonNode': 'candidateID', 'graphletSimilarity': 'glEmail-undir5'})
    gl0 = gl0[["templateID", "candidateID", "glEmail-undir5"]][gl0["candidateID"].isin(node_ids)]
    gl0.loc[:,"glEmail-undir5"] = 1 - gl0["glEmail-undir5"]
    distances = distances.merge(gl0, how="outer", on=["templateID", "candidateID"])
    gl1 = pd.read_csv(DISTANCE_FOLDER + "gl_0_undir5_similarity.csv")  # TemplateNode,PersonNode,graphletSimilarity
    gl1 = gl1.rename(columns={'TemplateNode': 'templateID', 'PersonNode': 'candidateID', 'graphletSimilarity': 'glPhone-undir5'})
    gl1 = gl1[["templateID", "candidateID", "glPhone-undir5"]][gl1["candidateID"].isin(node_ids)]
    gl1.loc[:,"glPhone-undir5"] = 1 - gl1["glPhone-undir5"]
    distances = distances.merge(gl1, how="outer", on=["templateID", "candidateID"])
    return distances

def clean_distance_cols(df):
    # Drop column that are not used
    col_to_drop = ["graphName", "templateType", "candidateType", "profileJaccard"]
    df = df.drop(columns=[c for c in col_to_drop if c in df.columns])
    return df

def process_distances(distances, subgraph_name, node_ids):
    df = clean_distance_cols(distances[distances["graphName"] == "Q1-" + subgraph_name])
    df = add_time_travel(df,"Q1-" + subgraph_name)
    # Make everything a distance [0,1], normalize from distances of all graphs
    return normalize_metric_values(df)

def process_candidates(template_edges, template_nodes):
    # Load files
    candidate_graph_names = ["Graph" + str(i) for i in range(1, 6)]
    all_nodes = pd.read_csv(DATA_FOLDER + "CGCS-GraphData-NodeTypes.csv")
    categories = pd.read_csv(DATA_FOLDER + "DemographicCategories.csv")

    edges_per_graph = {name: pd.read_csv(DATA_FOLDER + "Q1-" + name + ".csv") for name in candidate_graph_names}
    nodes_per_graph = {name: extract_subgraph_nodes(all_nodes, edges_per_graph[name]) for name in candidate_graph_names}
    graphs = {name: {"nodes": nodes_per_graph[name], "edges": edges_per_graph[name]} for name in candidate_graph_names}

    # Process distances
    distances = pd.read_csv(DISTANCE_FOLDER + "/distances_candidates.csv")
    distances_per_graph = {name: process_distances(distances, name, graphs[name]["nodes"]["NodeID"].unique())
                            for name in  candidate_graph_names}

    process_dataframes_candidate(template_edges, template_nodes, graphs, distances_per_graph, categories, DUMP_FOLDER)


def get_distance_list(subgraph_name, node_ids, path_subgraph=None, force_recompute_temporal=False, ifile_name=None):
    # Travel, demographic and graphlet distances for the complete graph
    if ifile_name is None:
        ifile_name = subgraph_name
    travel = pd.read_csv(
        DISTANCE_FOLDER + "travelJaccard.csv")  # TemplateNode,PersonNode,nTripTemplate,nTripPerson,travelJaccard
    travel = travel.rename(columns={'TemplateNode': 'templateID', 'PersonNode': 'candidateID'})
    travel = travel[["templateID", "candidateID", "travelJaccard"]][travel["candidateID"].isin(node_ids)]
    demographics = pd.read_csv(
        DISTANCE_FOLDER + "demographicsCosine.csv")  # TemplateNode,PersonNode,NumMatch,AvgDiff,Cosine,Pearson
    demographics = demographics.rename(
        columns={'TemplateNode': 'templateID', 'PersonNode': 'candidateID', 'Cosine': 'demographicsCosine'})
    demographics = demographics[["templateID", "candidateID", "demographicsCosine"]][
        demographics["candidateID"].isin(node_ids)]
    distance_merged = demographics.merge(travel, how="outer", on=["templateID", "candidateID"])
    distance_merged = add_graphlet(distance_merged, node_ids)

    # check if dtw distances are computed in distance folder
    distance_file_name = os.path.join(DISTANCE_FOLDER, "time_distances_all_channels", "dtw_" + subgraph_name + ".csv")

    if  force_recompute_temporal or not os.path.exists(distance_file_name):
        time_distances.compute_distances(path_subgraph, ifile_name, DISTANCE_FOLDER, output_name=subgraph_name)

    # Time distances only for the graph
    all_channels = pd.read_csv(
        os.path.join(DISTANCE_FOLDER, "time_distances_all_channels", "dtw_" + subgraph_name + ".csv"))  # TemplateId,Id,Distance
    all_channels = all_channels.rename(
        columns={'TemplateId': 'templateID', 'Id': 'candidateID', 'Distance': 'dtwAllEdges'})
    com_channels = pd.read_csv(
            os.path.join(
        DISTANCE_FOLDER, "time_distances_communication_channels", "dtw_" + subgraph_name + ".csv"))  # TemplateId,Id,Distance
    com_channels = com_channels.rename(
        columns={'TemplateId': 'templateID', 'Id': 'candidateID', 'Distance': 'dtwCommunication'})
    travel_channels = pd.read_csv(
            os.path.join(
        DISTANCE_FOLDER, "time_distances_travel_channel/", "dtw_" + subgraph_name + ".csv"))  # TemplateId,Id,Distance
    travel_channels = travel_channels.rename(
        columns={'TemplateId': 'templateID', 'Id': 'candidateID', 'Distance': 'dtwTravel'})

    distance_merged = distance_merged.merge(all_channels, how="outer", on=["templateID", "candidateID"])
    distance_merged = distance_merged.merge(com_channels, how="outer", on=["templateID", "candidateID"])
    distance_merged = distance_merged.merge(travel_channels, how="outer", on=["templateID", "candidateID"])
    return distance_merged

def add_already_paired(file_path, file_name, out_path ):

    src = os.path.join(file_path, f'paired_{file_name}.json')

    if os.path.exists(src):
        from shutil import copyfile
        dst = os.path.join(out_path, 'pairing.json')
        copyfile(src, dst)

def process_single(template_edges, template_nodes, options, force_recompute_temporal=False):
    CATEGORIES = pd.read_csv(DATA_FOLDER + "DemographicCategories.csv")

    for candidate_name, candidate_file in options:
        candidate_edges = pd.read_csv(candidate_file + ".csv")
        candidate_nodes = pd.read_csv(DATA_FOLDER + "CGCS-GraphData-NodeTypes.csv")
        candidate_nodes = extract_subgraph_nodes(candidate_nodes, candidate_edges)

        graph_folder = candidate_file.split('/')[-2]
        file_name = candidate_file.split('/')[-1]
        graph_name = file_name + "_" + graph_folder if "seed" in graph_folder else file_name
        distances = get_distance_list(graph_name, candidate_nodes["NodeID"].unique(), os.path.dirname(candidate_file),
                force_recompute_temporal, ifile_name=file_name)
        distances = normalize_metric_values(distances)

        candidate_nodes = annotate_nodes(candidate_nodes, candidate_edges, CATEGORIES)
        candidate_edges = normalize_timestamps(candidate_edges)

        dump_folder = os.path.join(DUMP_FOLDER, candidate_name)
        if not os.path.exists(dump_folder):
            os.makedirs(dump_folder)
        candidate_nodes.to_csv(os.path.join(dump_folder, 'candidate_nodes.csv'), index=False)
        candidate_edges.to_csv(os.path.join(dump_folder, 'candidate_edges.csv'), index=False)
        distances.to_csv(os.path.join(dump_folder, 'distances.csv'), index=False)

        add_already_paired(os.path.dirname(candidate_file), file_name, dump_folder)



def empty_data_folder():
    ok = input("About to wipe out content of folder '" + DUMP_FOLDER + "', press Enter to continue...")
    for root, dirs, files in os.walk(DUMP_FOLDER):
        for dir in dirs:
            shutil.rmtree(os.path.join(root, dir))


def process_template():
    CATEGORIES = pd.read_csv(DATA_FOLDER + "DemographicCategories.csv")
    template_edges = pd.read_csv(DATA_FOLDER + "CGCS-Template.csv")
    template_nodes = pd.read_csv(DATA_FOLDER + "CGCS-Template-NodeTypes.csv")
    # Add spending profile, demographic properties and flags
    template_nodes = annotate_nodes(template_nodes, template_edges, CATEGORIES)
    # Template edges will be further annotated for every candidate graph
    template_edges = normalize_timestamps(template_edges, is_template=True)

    #Create folder if it does not exist
    Path(DUMP_FOLDER).mkdir(parents=True, exist_ok=True)


    template_edges.to_csv(os.path.join(DUMP_FOLDER, 'template_edges.csv'), index=False)
    template_nodes.to_csv(os.path.join(DUMP_FOLDER, 'template_nodes.csv'), index=False)
    return template_edges, template_nodes

def add_seeds_and_matches_subgraphs(template_edges, template_nodes):
    MATCH_FOLDER = "../../python/extracted_matchs_seed/"

    OPTIONS = [
        ("seed1-travels-demo-time-greedy2",
         MATCH_FOLDER + "/seed1/match_travels_demographics_timeFilter_greedy2"),
        ("seed3-travels-demo-time-greedy2", MATCH_FOLDER + "/seed3/match_travels_demographics_timeFilter_greedy2"),
        ("best", DATA_FOLDER + "/final_matching/matched_nodes_with_demographics_filtered_with_node66")
    ]

    process_single(template_edges, template_nodes, OPTIONS)

def add_subgraph(graph_name, graph_path, force_recompute_temporal=False):
    template_edges, template_nodes = process_template()
    process_single(template_edges, template_nodes, [(graph_name, graph_path)], force_recompute_temporal)

import sys, getopt

def main(argv):
    input_file = ''
    output_file = ''
    force_recompute_temporal = False

    try:
      opts, args = getopt.getopt(argv,"hfi:o:",["ifile=","ofile="])
    except getopt.GetoptError:
      print('prepare_data.py -o <outputfile> -i <inputfile>')
      sys.exit(2)

    for opt, arg in opts:
      if opt == '-h':
         print('prepare_data.py -i <inputfile> -o <ouputfile>')
         sys.exit()
      elif opt in ("-i", "--ifile"):
         input_file = arg
      elif opt in ("-o", "--ofile"):
         output_file = arg
      elif opt == '-f':
          force_recompute_temporal = True
    print('Input file is "', input_file)
    print('Path is "', output_file)
    add_subgraph(output_file, input_file, force_recompute_temporal)

if __name__ == "__main__":
    if len(sys.argv) > 1 :
        main(sys.argv[1:])
    else:
        empty_data_folder()
        template_edges, template_nodes = process_template()
        process_candidates(template_edges, template_nodes)
        add_seeds_and_matches_subgraphs(template_edges, template_nodes)
