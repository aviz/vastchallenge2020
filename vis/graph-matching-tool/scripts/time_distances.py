
import datetime
import csv
import os

import pandas as pd
import numpy as np

from dtaidistance import dtw

def get_node_ts(df_graph, node_id, valid_etypes=[0, 1, 2, 3, 4, 6]):
    df_node = df_graph[(df_graph.eType.isin(valid_etypes)) & 
                       ((df_graph.Source==node_id) | 
                        (df_graph.Target==node_id))
                      ][["Time"]]
    if df_node.shape[0]==0:
        return None
    #[["Source", "Target", "Time", "Weight", "SourceLocation", "TargetLocation"]]
    node_ts = df_node[["Time"]].resample('D', on='Time').agg('size').reset_index()
    node_ts.columns=["Time", "Count"]
    return node_ts.Count.values.astype('float')


def get_all_ts(df_graph, valid_etypes=[0, 1, 2, 3, 4, 6]):
    node_ids = np.unique(np.concatenate((df_graph.Source.values, df_graph.Source.values)))

    graph_ts = []
    graph_ids = []
    for i, nt in enumerate(node_ids):
        if i % 100 == 0:
            print(i)
        tst = get_node_ts(df_graph, nt, valid_etypes)
        if tst is not None:
            graph_ts.append(tst)
            graph_ids.append(nt)
    return graph_ids, graph_ts

def dists_ts(ts1, ts2, window=3):
    series_t_g = ts1 + ts2
    block = ((0, len(ts1)), (len(ts1), len(series_t_g)))

    dm = dtw.distance_matrix_fast(series_t_g, block=block, window=window, psi=2)
    distances = dm[block[0][0]:block[0][1], block[1][0]:block[1][1]]
    return distances

def create_paths(distances_path):
    from pathlib import Path

    out_path_all = os.path.join(distances_path, 'time_distances_all_channels')
    Path(out_path_all).mkdir(parents=True, exist_ok=True)
    
    out_path_communication = os.path.join(distances_path, 'time_distances_communication_channels')
    Path(out_path_communication).mkdir(parents=True, exist_ok=True)

    out_path_travel = os.path.join(distances_path, 'time_distances_travel_channel')
    Path(out_path_travel).mkdir(parents=True, exist_ok=True)

    list_paths = [out_path_all, out_path_communication, out_path_travel]
    
    return list_paths

    
def read_graph(graph_path, graph_name):



    origin = datetime.datetime(2025, 1, 1, 0, 0)

    filename = os.path.join(graph_path, graph_name+'.csv')

    df_g2 = pd.read_csv(filename)

    if df_g2['Time'].dtype == 'int64':
        df_g2['Time'] = pd.to_datetime(df_g2['Time'], unit='s', origin=pd.to_datetime(origin)) 
    else:
        df_g2['Time'] = pd.to_datetime(df_g2['Time']) 
    return df_g2
    
    
    
def compute_and_save_distances(template_graph_ts, tg_ids, valid_etypes, 
                               graph_path, graph_name, out_path,
                              save_name=None):
    if save_name is None:
        save_name = graph_name
    
    print(graph_path)
    print(graph_name)

    df_g2 = read_graph(graph_path, graph_name)
    
    g2_ids, g2_ts = get_all_ts(df_g2, valid_etypes)
    
    distances = dists_ts(template_graph_ts, g2_ts, window=3)

    with open(f'{out_path}/dtw_{save_name}.csv', 'w') as csvfile:
        writer = csv.writer(csvfile)
        row = ['TemplateId', 'Id', 'Distance']
        writer.writerow(row)
        i = 0
        for sn in tg_ids:
            j=0
            for gn in g2_ids:
                row = [sn, gn, distances[i][j]]
                writer.writerow(row)
                j += 1
            i += 1
            
            
def read_template(path_template):
    
    nodes_file_template = "CGCS-Template-NodeTypes.csv"
    edges_file_template = "CGCS-Template.csv"

    origin = datetime.datetime(2025, 1, 1, 0, 0)

    df_template = pd.read_csv(os.path.join(path_template, edges_file_template))
    df_template['Time'] = df_template['Time']+1209600
    df_template['Time'] = pd.to_datetime(df_template['Time'], unit='s', origin=pd.to_datetime(origin)) 
    
    return df_template


def compute_distances(path_subgraph, subgraph_name, DISTANCE_FOLDER, path_template="../../data", output_name=None):  
   
    if output_name is None:
        output_name = subgraph_name

    print(path_subgraph)
    print(subgraph_name)

    list_paths = create_paths(DISTANCE_FOLDER)
    
    valid_etypes_all=[0, 1, 2, 3, 4, 6]
    valid_etypes_communication=[0, 1]
    valid_etypes_travel=[6]
    
    list_valid = [valid_etypes_all, valid_etypes_communication, valid_etypes_travel]

    df_template = read_template(path_template)
    
    for valid_etypes, out_path in zip(list_valid, list_paths):

        tg_ids, tg_ts = get_all_ts(df_template, valid_etypes)

        compute_and_save_distances(tg_ts, tg_ids, valid_etypes, 
                                   path_subgraph, subgraph_name, out_path,
                                  save_name=f'{output_name}')

