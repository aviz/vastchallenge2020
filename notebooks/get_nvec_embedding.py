import get_files
from nodevectors import Node2Vec
import numpy as np

com_mat = get_files.get_communication_matrix().tocsr() 


# Fit embedding model to graph
g2v = Node2Vec(n_components=2)
# way faster than other node2vec implementations
# Graph edge weights are handled automatically
g2v.fit(com_mat)

embedding = np.array([g2v.predict(n) for n in range(com_mat.shape[0])])

np.save('nvec_embedding.npy', embedding)
