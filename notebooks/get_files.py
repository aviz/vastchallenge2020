import pyarrow.parquet as pq
import numpy as np
from scipy import sparse 
import os

def load_column(column, path='/home/vast/', infilename='CGCS-GraphData.parquet'):
    return pq.read_table(path + infilename, columns=[column]).to_pandas()

def load_columns(columns, path='/home/vast/', infilename='CGCS-GraphData.parquet', filters=None):
    return pq.read_table(path + infilename, columns=columns, use_legacy_dataset=False, filters=filters).to_pandas()

def get_numpy_array_from_df(columns, column, filepath, save=True):
    if column == columns.index.name:
        values = columns.index.values.reshape(-1)
    else:
        values = columns[column].values.reshape(-1)
        
    if save: 
        with open(filepath, 'w') as out:
                values.tofile(out)
    return values

def get_communication_columns(path='./', channel=None, save=True):   
    column_source = 'Source'
    column_target = 'Target'
    column_type = 'eType'
    print(column_type)
    
    
    if channel is None:
        filename_source = column_source+'_communication.npy'
        filename_target = column_target+'_communication.npy'
    elif channel == 'all':
        filename_source = f'{column_source}_communication_all.npy'
        filename_target = f'{column_target}_communication_all.npy'
    else:
        filename_source = f'{column_source}_communication_{channel}.npy'
        filename_target = f'{column_target}_communication_{channel}.npy'
        
    filepath_source = os.path.join(path, filename_source)
    filepath_target = os.path.join(path, filename_target)

        
    if os.path.exists(filepath_source) and os.path.exists(filepath_target):
        print('Loading already computed.')
        return (
            np.fromfile(filepath_source, dtype=np.int64),
            np.fromfile(filepath_target, dtype=np.int64))
      
    if channel is None:
        filters=[(column_type,'<', 2)]
    elif channel == 'all':
        filters=None
    else:
        filters=[(column_type,'==', channel)]
        
    print(f'Getting columns for channel {channel}.')

    columns = load_columns(columns=[column_source, column_target, column_type], filters=filters)

    if channel is None:
        columns = columns[columns[column_type] < 2]
    elif channel == 'all':
        columns = columns
    else:
        columns = columns[columns[column_type] == channel]
       
    
    source_array = get_numpy_array_from_df(columns, column_source, filepath_source, save=save)
    target_array = get_numpy_array_from_df(columns, column_target, filepath_target, save=save)
     
    return (source_array, target_array)

def get_column(column, path='./', channel=None):
    if channel is None:
        filename = column+'_communication.npy'
    else:
        filename = f'{column}_communication_{channel}.npy'
        
    filepath = os.path.join(path, filename)
    if not os.path.exists(filepath):
        print('Column not saved, compute and save it calling get_communication_columns')
        return None
    return np.fromfile(filepath, dtype=np.int64)

def get_source_communication(path='./', channel=None):
    column = 'Source'
    return get_column(column, path, channel)

def get_target_communication(path='./', channel=None):
    column = 'Target'
    return get_column(column, path, channel)


def get_communication_unique(source_communication=None,
                             target_communication=None,
                             channel=None,
                             path='./', save=True):
    if channel is None:
        filename = 'communication_unique.npy'
    else:
        filename = f'communication_unique_{channel}.npy'
        
    filepath = os.path.join(path, filename)
    
    if os.path.exists(filepath):
        print('Loading already computed.')
        return np.fromfile(filepath, dtype=np.int64)
    
    if source_communication is None:
        source_communication = get_source_communication(path=path, channel=channel)
    if source_communication is None:
        source_communication, target_communication = get_communication_columns(channel=channel, 
                                                                               path=path, 
                                                                               save=save)
    
    if target_communication is None:
        target_communication = get_target_communication(path=path, channel=channel)
    if target_communication is None:
        return None
    
    nodes_unique = np.unique(np.concatenate((source_communication, target_communication)))
    with open(filepath, 'w') as out:
        nodes_unique.tofile(out)
        
    return nodes_unique

def get_source_communication_matrix_index(source_communication=None,
                                          nodes_unique=None,
                                          target_communication=None,
                                          channel=None,
                                          path='./', 
                                          save=True):
    
    if channel is None:
        filename = 'source_communication_matrix_index.npy'
    else:
        filename = f'source_communication_matrix_index_{channel}.npy'
        
    filepath = os.path.join(path, filename)

    if os.path.exists(filepath):
        print('Loading already computed source_communication_matrix_index.')
        return np.fromfile(filepath, dtype=np.int64)

    
    if nodes_unique is None:
        nodes_unique = get_communication_unique(source_communication=source_communication, 
                                                target_communication=target_communication, 
                                                channel=channel,
                                                path=path, 
                                                save=save)
    if source_communication is None:
        source_communication = get_source_communication(path=path, channel=channel)
    if source_communication is None:
        source_communication, _ = get_communication_columns(channel=channel, 
                                                           path=path, 
                                                           save=save)

    source_communication_index = np.searchsorted(nodes_unique, source_communication, side='left')
                
    if save:
        with open(filepath, 'w') as out:
            source_communication_index.tofile(out)
    return source_communication_index

def get_target_communication_matrix_index(source_communication=None,
                                          nodes_unique=None,
                                          target_communication=None,
                                          channel=None,
                                          path='./', 
                                          save=True):
    
    if channel is None:
        filename = 'target_communication_matrix_index.npy'
    else:
        filename = f'target_communication_matrix_index_{channel}.npy'
        
    filepath = os.path.join(path, filename)

    if os.path.exists(filepath):
        print('Loading already computed target_communication_matrix_index.')
        return np.fromfile(filepath, dtype=np.int64)

    
    if nodes_unique is None:
        nodes_unique = get_communication_unique(source_communication=source_communication, 
                                                target_communication=target_communication, 
                                                channel=channel,
                                                path=path, 
                                                save=save)
    if target_communication is None:
        target_communication = get_target_communication(path=path, channel=channel)
    if target_communication is None:
        _, target_communication = get_communication_columns(channel=channel, 
                                                           path=path, 
                                                           save=save)

    target_communication_index = np.searchsorted(nodes_unique, target_communication, side='left')
                
    if save:
        with open(filepath, 'w') as out:
            target_communication_index.tofile(out)
    return target_communication_index

def get_communication_matrix(source_communication_index=None, 
                             target_communication_index=None,
                             channel=None,
                             path='./', save=True):
    if channel is None:
        filename = 'communication_matrix.npz'
    else:
        filename = f'communication_matrix_{channel}.npz'
        
    filepath = os.path.join(path, filename)
    if os.path.exists(filepath):
        print(f'Loading already computed matrix for channel {channel}.')
        return sparse.load_npz(filepath)
    
    
    print(f'Computing source/target nodes for channel {channel}.')
    source_communication, target_communication = get_communication_columns(channel=channel, 
                                                                               path=path, 
                                                                               save=save)
    
    print(f'Computing nodes unique for channel {channel}.')
    nodes_unique = get_communication_unique(source_communication=source_communication,
                                                      target_communication=target_communication,
                                                      channel=channel)
    N = nodes_unique.shape[0]
    
    if source_communication_index is None:
        print(f'Computing source unique indices for channel {channel}.')
        source_communication_index = get_source_communication_matrix_index(source_communication=source_communication,
                                                                           nodes_unique=nodes_unique,
                                                                           channel=channel)
        
    if target_communication_index is None:
        print(f'Computing target unique indices for channel {channel}.')
        target_communication_index = get_target_communication_matrix_index(target_communication=target_communication,
                                                                           nodes_unique=nodes_unique,
                                                                           channel=channel)
    print(f'Build matrix for channel {channel}.')
    
    coo_matrix = sparse.coo_matrix(
        (np.ones((len(source_communication_index),), 
                 dtype=np.int32), 
         (source_communication_index, target_communication_index)), 
        shape=(N,N), dtype=np.int32)
    coo_matrix.sum_duplicates()
    if save:
        sparse.save_npz(filepath, coo_matrix) 
    return coo_matrix

