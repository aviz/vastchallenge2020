from scipy import sparse 
import numpy as np
import get_files
from scipy.sparse import csgraph
from scipy.sparse import linalg



com_mat = get_files.get_communication_matrix().tocsr() 
com_mat += com_mat.transpose()

L = csgraph.laplacian(com_mat, normed=False)
e, embedding = sparse.linalg.eigsh(L.astype(float), k=3, which="SM")

np.save('spec_embedding_e.npy', embedding)

