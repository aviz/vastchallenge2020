from sknetwork.embedding import Spectral
import numpy as np
import get_files


com_mat = get_files.get_communication_matrix().tocsr() 
com_mat += com_mat.transpose()

spectral = Spectral(normalized_laplacian=False,normalized=False,solver='lanczos')
embedding = spectral.fit_transform(com_mat)
np.save('spec_embedding.npy', embedding)

