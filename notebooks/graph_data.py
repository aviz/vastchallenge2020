from datetime import datetime
import pandas as pd
import altair as alt


class GraphData: 
    
    def __init__(self):
        self.nodes = None
        self.edges = None
        
        self.nodes_sort = None
        
        self.dem_categories = self.get_dem_categories()
        self.nodeTypes = self.get_node_types()
        self.edgeTypes = self.get_edge_types()
        
        self.scaleEtype = None
        self.scaleStType = None
        self.colorEType = None
        self.colorStType = None
        
        self.edges_financial = None
        self.edges_no_financial = None
        
        self.time_domain = None
    
    def dateparse(self, time_in_secs):    
        years = 2025-1970
        d = datetime.fromtimestamp(float(time_in_secs))
        return d.replace(year = d.year + years)
    
   
    def get_dem_categories(self):
        dem_dict = {459381: 'Water and other public services',
             466907: 'Electricity',
             473173: 'Household furnishings',
             503218: 'Natural gas',
             503701: 'Miscellaneous',
             510031: 'Gifts',
             520660: 'Healthcare',
             523927: 'Restaurants',
             527449: 'Alcohol',
             536346: 'Home maintenance',
             537281: 'Housekeeping supplies',
             552988: 'Money income before taxes',
             567195: 'Personal insurance and pensions',
             571970: 'Reading',
             575030: 'Transportation',
             577992: 'Education',
             580426: 'Telephone services',
             589943: 'Lodging away from home',
             595298: 'Groceries',
             595581: 'Donations',
             606730: 'Entertainment',
             616315: 'Apparel and services',
             620120: 'Personal taxes',
             621924: 'Mortgage payments',
             630626: 'Rented dwellings',
             632961: 'Personal care products and services',
             640784: 'Tobacco',
             642329: 'Household operations',
             644226: 'Property taxes'}
        return dem_dict
    
    def get_node_types(self):
        return {1: "Person", 2: "Product", 3: "Document", 4: "Financial", 5: "Country"}
    
    def get_edge_types(self):
        return {0: "Email", 1: "Phone", 2: "Sell", 3: "Buy", 4: "Author-of", 5: "Financial", 6: "Travels-to"}
     
    def get_time_extent(self, exclude_documents=False):
        dt_pandas_to_alt = lambda dt: alt.DateTime(year=dt.year, month=dt.month, date=dt.day)

        if exclude_documents:
            min_dt = self.edges.loc[self.edges.eType != 'Author-of'].Time.min()
            max_dt = self.edges.loc[self.edges.eType != 'Author-of'].Time.max()
        else:
            min_dt = self.edges.Time.min()
            max_dt = self.edges.Time.max()

        extent_dt = [dt_pandas_to_alt(min_dt), dt_pandas_to_alt(max_dt)]
        return extent_dt
    
    def create_aux(self, stList=None):
        if stList is None:
            stList =  self.edges.stType.unique()

        self.nodes_sort = self.nodes.sort_values(by=['NodeType', 'NodeID']).NodeID.to_list()
        
        self.scaleEtype = alt.Scale(domain=list(self.edgeTypes.values()))
        self.scaleStType= alt.Scale(domain=stList)

        self.colorEType = alt.Color('eType:N', scale=self.scaleEtype )
        self.colorStType = alt.Color('stType:N', scale=self.scaleStType)
        self.edges_financial = self.edges.loc[(self.edges.SourceType=='Financial') 
                                              | (self.edges.TargetType=='Financial') ]
        self.edges_no_financial = self.edges.loc[(self.edges.SourceType!='Financial') 
                                              & (self.edges.TargetType!='Financial') ]
        
        nodes_financial_weight = self.edges_financial.groupby(['Source']).Weight.sum()
        self.edges['SumWeight'] = self.edges["Source"].apply(
            lambda n: nodes_financial_weight[n] if n in nodes_financial_weight else 0)
        
        self.time_domain = self.get_time_extent()

    def make_replacements(self):
       
        node_replacements = self.nodeTypes
        edge_replacements = self.edgeTypes
        dem_replacements  = self.dem_categories
        
        replace_dem =  lambda x: dem_replacements[x] if x in dem_replacements else str(x)
        
        self.nodes = self.nodes[self.nodes['NodeID'].isin(set(self.edges.Source).union(set(self.edges.Target)))]
        
        self.nodes["NodeType"] =  self.nodes["NodeType"].replace(node_replacements)
        self.edges["eType"] = self.edges["eType"].replace(edge_replacements)        
        
        self.edges = self.edges.merge(self.nodes, how="left", left_on="Source", right_on="NodeID"
                                     ).rename(columns={"NodeType": "SourceType"}
                                             ).drop("NodeID", axis=1)
        self.edges = self.edges.merge(self.nodes, how="left", left_on="Target", right_on="NodeID"
                                     ).rename(columns={"NodeType": "TargetType"}
                                             ).drop("NodeID", axis=1)

        self.edges["Source"] = self.edges["Source"].apply(replace_dem)
        self.edges["Target"] = self.edges["Target"].apply(replace_dem)
        self.nodes["NodeID"] = self.nodes["NodeID"].apply(replace_dem)

        # replace travel target
        masktravel =  self.edges.eType=='Travels-to'
        self.edges.loc[masktravel, "Target"] = 'c-' + self.edges.loc[masktravel, "TargetLocation"].astype(str)

        
        self.edges["stType"] = self.edges["SourceType"] + "-> " + self.edges["TargetType"]
        
    def read_data(self, nodes_file, edges_file, path='./', path_nodes='./',  stList=None, parseTime=True):
    
        self.nodes = pd.read_csv(path_nodes + nodes_file)
        if parseTime:
            self.edges = pd.read_csv(path + edges_file, parse_dates=['Time'], date_parser=self.dateparse)
        else:
            self.edges = pd.read_csv(path + edges_file, parse_dates=['Time'])
        
        self.edges.fillna({
            'SourceLocation': -1, 'TargetLocation': -1
        }, inplace=True)
        self.edges = self.edges.astype({
            'SourceLocation': 'int32', 'TargetLocation': 'int32'
        }, copy=False, errors='ignore')


        self.make_replacements()   
        self.create_aux(stList)

    def sort_leaf_order(self):
        from scipy.spatial import distance
        from scipy.cluster import hierarchy

        edges_no_financial = edges.loc[(edges.SourceType!='Financial') & (edges.TargetType!='Financial') ]
        matrix_dict = edges_no_financial.groupby(['Source', 'Target']).SourceType.count().to_dict()

        nodes_no_financial = nodes[nodes.NodeType!='Financial']
        nodes_list = nodes_no_financial.NodeID.tolist()
        N = len(nodes_list)

        nodes_map_ids_i = dict(zip(range(N), nodes_list))
        nodes_map_ids = dict(zip(nodes_list, range(N)))

        matrix = np.zeros(shape=(N, N))
        m_inf = max(matrix_dict.values())*2

        for e, w in matrix_dict.items():
            matrix[nodes_map_ids[e[0]], nodes_map_ids[e[1]]] = w
            matrix[nodes_map_ids[e[1]], nodes_map_ids[e[0]]] = w

        matrix[matrix==0] = 3*m_inf
        for i in range(N):
            matrix[i,i] = 0 

        matrix_condensed = distance.squareform(matrix, force='tovector')

        Z = hierarchy.linkage(matrix_condensed)
        leaves = hierarchy.leaves_list(hierarchy.optimal_leaf_ordering(Z, matrix_condensed))

        new_nodes_sort = [nodes_map_ids_i[l] for l in leaves]
