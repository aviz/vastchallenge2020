from sknetwork.embedding import Spring
import numpy as np
import get_files


com_mat = get_files.get_communication_matrix().tocsr() 
com_mat += com_mat.transpose()

spring = Spring()
embedding = spring.fit_transform(com_mat)
np.save('spring_embedding.npy', embedding)

