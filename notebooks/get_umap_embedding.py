from sknetwork.embedding import Spectral
import get_files
import umap
import numpy as np

com_mat = get_files.get_communication_matrix().tocsr() 

reducer = umap.UMAP(metric='cosine',verbose=True)
embedding = reducer.fit_transform(com_mat)
np.save('umap_embedding.npy', embedding)
