#!/bin/bash

#N=3

#if [ ! -f Q1-Graph1.csv ]; then
#    echo "Missing graph Q1-Graph1.csv" 1>&2
#    exit 1
#fi

#if [ ! -f dir$N.str ]; then
#    echo "Missing motif file dir$N.str" 1>&2
#    exit 1
#fi

# Extract type=0 edges
#python filtercsv.py Q1-Graph1.csv 0 > Q1-Graph1-0.csv

# Compute motifs matching for directed graph 0
#../gtrieScanner -s $N -m subgraphs dir$N.str -g Q1-Graph1-0.csv -d -f csv -oc Q1-Graph1-0.dump > /dev/null
#python dump2vec.py Q1-Graph1-0.dump > Q1-Graph1-0.csv


# Candidate graph loop
for graph in 1 2 3 4 5
do
    if [ ! -f Q1-Graph$graph.csv ]; then
        echo "Missing graph Q1-Graph$graph.csv" 1>&2
        exit 1
    fi
    # edge type loop
    for edgeType in 0 1 2 3 4 5 6
    do
	# sizes of undirected graphlets
        for n in 3 4 5
        do
            python filtercsv.py Q1-Graph$graph.csv $edgeType > Q1-Graph$graph-$edgeType-undir$n.csv
	    ../gtrieScanner -s $n -m subgraphs undir$n.str -g Q1-Graph$graph-$edgeType-undir$n.csv -u -f csv -oc Q1-Graph$graph-$edgeType-undir$n.dump > /dev/null
            python dump2vec.py Q1-Graph$graph-$edgeType-undir$n.dump > Q1-Graph$graph-$edgeType-undir$n.csv
        done
	# sizes of directed graphlets
        for n in 3 4
        do
            python filtercsv.py Q1-Graph$graph.csv $edgeType > Q1-Graph$graph-$edgeType-dir$n.csv
            ../gtrieScanner -s $n -m subgraphs dir$n.str -g Q1-Graph$graph-$edgeType-dir$n.csv -d -f csv -oc Q1-Graph$graph-$edgeType-dir$n.dump > /dev/null
            python dump2vec.py Q1-Graph$graph-$edgeType-dir$n.dump > Q1-Graph$graph-$edgeType-dir$n.csv
        done
    done
done


# template graph loop
for edgeType in 0 1 2 3 4 5 6
do
    # sizes of undirected graphlets
    for n in 3 4 5
    do
        python filtercsv.py CGCS-Template.csv $edgeType > template-$edgeType-undir$n.csv
        ../gtrieScanner -s $n -m subgraphs undir$n.str -g template-$edgeType-undir$n.csv -u -f csv -oc template-$edgeType-undir$n.dump > /dev/null
        python dump2vec.py template-$edgeType-undir$n.dump > template-$edgeType-undir$n.csv
    done
    # sizes of directed graphlets
    for n in 3 4
    do
        python filtercsv.py CGCS-Template.csv $edgeType > template-$edgeType-dir$n.csv
        ../gtrieScanner -s $n -m subgraphs dir$n.str -g template-$edgeType-dir$n.csv -d -f csv -oc template-$edgeType-dir$n.dump > /dev/null
        python dump2vec.py template-$edgeType-dir$n.dump > template-$edgeType-dir$n.csv
    done
done



