
start=`date +%s`

python filtercsv.py /home/vast/CGCS-GraphData.csv 0 > bigGraph-0-undir5.csv
../gtrieScanner -s 5 -m subgraphs undir5.str -g bigGraph-0-undir5.csv -u -f csv -oc bigGraph-0-undir5.dump > /dev/null
python dump2vec.py bigGraph-0-undir5.dump > bigGraph-0-undir5.csv

end=`date +%s`
runtime=$((end-start))

echo $runtime > time_bigGraph_undir5_0.txt
