#!/bin/env python3

import urllib.request
import ssl
import os
import sys
import bisect

FILES = [
    'https://www.dcc.fc.up.pt/gtries/gtries.zip',
    'https://www.dcc.fc.up.pt/gtries/lists.zip',
    'https://www.dcc.fc.up.pt/gtries/s420_st.txt',
    'https://www.dcc.fc.up.pt/gtries/yeastInter_st.txt'
    ]


def feedback(cnt, blksize, totalsize):
    if cnt == 0:
        return
    percents = [totalsize*pc/100 for pc in range(5, 100, 5)]
    prev = (cnt-1)*blksize
    cur = cnt*blksize
    if (cnt % 128) == 0:
        print('.', file=sys.stderr, end='')
    if bisect.bisect(percents, prev) != bisect.bisect(percents, cur):
        print(f"{cur:,}/{totalsize:,}", file=sys.stderr)


def loadurl(url):
    filename = url[url.rindex('/')+1:]
    if os.path.isfile(filename):
        print(f"`{filename}` is already downloaded", file=sys.stderr)
        return
    ssl._create_default_https_context = ssl._create_unverified_context
    print(f"Downloading `{filename}`", file=sys.stderr)
    local_filename, headers = urllib.request.urlretrieve(url,
                                                         None,
                                                         reporthook=feedback)
    print('done.', file=sys.stderr)
    os.rename(local_filename, filename)


if __name__ == "__main__":
    for filename in FILES:
        loadurl(filename)
