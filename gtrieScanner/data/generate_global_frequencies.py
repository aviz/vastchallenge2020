import os
import random

import pandas as pd
import numpy as np

folder1 = os.getcwd() + '/frequencies'
folder2 = '/home/apister/vastchallenge2020/python/extracted_graphs2/graphlets'
folder3 = '/home/apister/vastchallenge2020/python/induced_graphs/graphlets'
folder4 = './'

for folder in [folder4]: # [folder1, folder2, folder3]:
    for file in os.listdir(folder):
        if file.endswith(".csv") and 'big' not in file and 'final' in file:
            graphlet_size = int(file[-5:-4])
            print(graphlet_size)
        
            print(file)
            file_abs = os.path.join(folder, file)
            df = pd.read_csv(file_abs, index_col='Vertex')
        
            if 'graph' not in df.index:
                df.loc['graph'] = df.sum() / graphlet_size
                df.to_csv(file_abs)


