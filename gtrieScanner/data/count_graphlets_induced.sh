#!/bin/bash

#N=3

#if [ ! -f Q1-Graph1.csv ]; then
#    echo "Missing graph Q1-Graph1.csv" 1>&2
#    exit 1
#fi

#if [ ! -f dir$N.str ]; then
#    echo "Missing motif file dir$N.str" 1>&2
#    exit 1
#fi

# Extract type=0 edges
#python filtercsv.py Q1-Graph1.csv 0 > Q1-Graph1-0.csv

# Compute motifs matching for directed graph 0
#../gtrieScanner -s $N -m subgraphs dir$N.str -g Q1-Graph1-0.csv -d -f csv -oc Q1-Graph1-0.dump > /dev/null
#python dump2vec.py Q1-Graph1-0.dump > Q1-Graph1-0.csv


for entry in /home/apister/vastchallenge2020/python/induced_graphs/*
do
  if [ ${entry: -4} == ".csv" ]; then
    echo $entry
    graph=$(basename -- $entry .csv)
    echo $graph
    #python filtercsv.py entry > extracted_frequencies/Q1-Graph$graph-$edgeType-undir$n.csv
    ../gtrieScanner -s 5 -m subgraphs undir5.str -g $entry -u -f csv -oc /home/apister/vastchallenge2020/python/induced_graphs/graphlets/$graph-0-undir5.dump
    python dump2vec.py /home/apister/vastchallenge2020/python/induced_graphs/graphlets/$graph-0-undir5.dump > /home/apister/vastchallenge2020/python/induced_graphs/graphlets/$graph-0-undir5.csv

    ../gtrieScanner -s 4 -m subgraphs dir4.str -g $entry -u -f csv -oc /home/apister/vastchallenge2020/python/induced_graphs/graphlets/$graph-0-dir4.dump
    python dump2vec.py /home/apister/vastchallenge2020/python/induced_graphs/graphlets/$graph-0-dir4.dump > /home/apister/vastchallenge2020/python/induced_graphs/graphlets/$graph-0-dir4.csv

  fi
done
