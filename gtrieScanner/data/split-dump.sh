#!/bin/sh

#set -x

dirname=`basename $1 .dump`

mkdir -p $dirname
cd $dirname
split -a 4 --additional-suffix=.dump -l 10000 ../$1

for dump in *.dump;
do
    python ../dump2vec.py $dump > `basename $dump .dump`.csv
done

