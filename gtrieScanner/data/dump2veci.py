"Converts incrementally a dump file into a vector file"

from collections import defaultdict, Counter
import sys
# from pprint import pprint


def dump(outfilename, motifs, vertex_motif):
    print("Dumping in ", outfilename)
    with open(outfilename, 'w') as outfile:
        print("Vertex,", end='', file=outfile)
        print(*motifs, sep=',', file=outfile)
        for vertex, counts in vertex_motif.items():
            print(vertex, end='', file=outfile)
            for motif in motifs:
                print(',', counts[motif], end='', file=outfile)
            print(file=outfile)


def process_dump(filename, dump_every=100000):
    prefix = filename[:filename.rindex('.')]
    vertex_motif = defaultdict(Counter)
    filenum = 1
    expect = 0
    motifs = set()
    with open(filename, 'r') as infile:
        lines = 0
        while True:
            line = infile.readline().rstrip('\n\r')
            if not line:
                break
            lines += 1
            if lines >= dump_every:
                lines = 0
                outfilename = f"{prefix}-{filenum}.csv"
                filenum += 1
                dump(outfilename, motifs, vertex_motif)
                motifs = set()
                vertex_motif = defaultdict(Counter)
            field = line.split(' ')
            if expect == 0:
                expect = len(field)
            elif expect != len(field):
                print(f"Skipping incomplete line {lines}", file=sys.stderr)
                continue
            motif = field[0][:-1]
            motifs.add(motif)
            for vertex in field[1:]:
                vertex_motif[int(vertex)][motif] += 1
    if motifs:
        outfilename = f"{prefix}-{filenum}.csv"
        dump(outfilename, motifs, vertex_motif)


if __name__ == "__main__":
    ARGC = len(sys.argv)
    DUMP_EVERY = 100000
    if ARGC < 2:
        print(f"syntax: {sys.argv[0]} <dump file> [dump_every]",
              file=sys.stderr)
        sys.exit(1)
    elif ARGC > 2:
        DUMP_EVERY = int(sys.argv[2])
    process_dump(sys.argv[1], DUMP_EVERY)
