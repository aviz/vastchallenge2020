for graph in 1 2 3 4 5
do
	python filtercsv.py Q1-Graph$graph.csv 0 1 > Q1-Graph$graph-0-1-undir5.csv
	../gtrieScanner -s 5 -m subgraphs undir5.str -g Q1-Graph$graph-0-1-undir5.csv -u -f csv -oc Q1-Graph$graph-0-1-undir5.dump > /dev/null
	python dump2vec.py Q1-Graph$graph-0-1-undir5.dump > Q1-Graph$graph-0-1-undir5.csv

        python filtercsv.py Q1-Graph$graph.csv 0 1 > Q1-Graph$graph-0-1-dir4.csv
        ../gtrieScanner -s 4 -m subgraphs dir4.str -g Q1-Graph$graph-0-1-dir4.csv -d -f csv -oc Q1-Graph$graph-0-1-dir4.dump > /dev/null
        python dump2vec.py Q1-Graph$graph-0-1-dir4.dump > Q1-Graph$graph-0-1-dir4.csv

        python filtercsv.py Q1-Graph$graph.csv 2 3 > Q1-Graph$graph-2-3-undir5.csv
        ../gtrieScanner -s 5 -m subgraphs undir5.str -g Q1-Graph$graph-2-3-undir5.csv -u -f csv -oc Q1-Graph$graph-2-3-undir5.dump > /dev/null
        python dump2vec.py Q1-Graph$graph-2-3-undir5.dump > Q1-Graph$graph-2-3-undir5.csv

        python filtercsv.py Q1-Graph$graph.csv 2 3 > Q1-Graph$graph-2-3-dir4.csv
        ../gtrieScanner -s 4 -m subgraphs dir4.str -g Q1-Graph$graph-2-3-dir4.csv -d -f csv -oc Q1-Graph$graph-2-3-dir4.dump > /dev/null
        python dump2vec.py Q1-Graph$graph-2-3-dir4.dump > Q1-Graph$graph-2-3-dir4.csv
done

python filtercsv.py CGCS-Template.csv 0 1 > template-0-1-undir5.csv
../gtrieScanner -s 5 -m subgraphs undir5.str -g template-0-1-undir5.csv -u -f csv -oc template-0-1-undir5.dump > /dev/null
python dump2vec.py template-0-1-undir5.dump > template-0-1-undir5.csv

python filtercsv.py CGCS-Template.csv 0 1 > template-0-1-dir4.csv
../gtrieScanner -s 4 -m subgraphs dir4.str -g template-0-1-dir4.csv -d -f csv -oc template-0-1-dir4.dump > /dev/null
python dump2vec.py template-0-1-dir4.dump > template-0-1-dir4.csv

python filtercsv.py CGCS-Template.csv 2 3 > template-2-3-undir5.csv
../gtrieScanner -s 5 -m subgraphs undir5.str -g template-0-1-undir5.csv -u -f csv -oc template-2-3-undir5.dump > /dev/null
python dump2vec.py template-2-3-undir5.dump > template-2-3-undir5.csv

python filtercsv.py CGCS-Template.csv 2 3 > template-2-3-dir4.csv
../gtrieScanner -s 4 -m subgraphs dir4.str -g template-2-3-dir4.csv -d -f csv -oc template-2-3-dir4.dump > /dev/null
python dump2vec.py template-2-3-dir4.dump > template-2-3-dir4.csv
