for graph in 1 2 3 4 5
do
	for graphlet in dir undir
	do
		if [[ "${graphlet}" == "undir" ]]
		then
			graphlets=( 3 4 5 )
			flag=u
		fi
		if [[ "${graphlet}" == "dir" ]]
		then
			graphlets=( 3 4 )
			flag=d
		fi 
		for N in ${graphlets[@]}
		do
			echo $N
			echo $graphlet$N
			python filtercsv.py Q1-Graph$graph.csv 0,1 > frequencies/Q1-Graph$graph-0-1-$graphlet$N.csv
			../gtrieScanner -s $N -m subgraphs $graphlet$N.str -g frequencies/Q1-Graph$graph-0-1-$graphlet$N.csv -$flag -f csv -oc frequencies/Q1-Graph$graph-0-1-$graphlet$N.dump > /dev/null
			python dump2vec.py frequencies/Q1-Graph$graph-0-1-$graphlet$N.dump > frequencies/Q1-Graph$graph-0-1-$graphlet$N.csv

			python filtercsv.py Q1-Graph$graph.csv 2,3 > frequencies/Q1-Graph$graph-2-3-$graphlet$N.csv
			../gtrieScanner -s $N -m subgraphs $graphlet$N.str -g frequencies/Q1-Graph$graph-2-3-$graphlet$N.csv -$flag -f csv -oc frequencies/Q1-Graph$graph-2-3-$graphlet$N.dump > /dev/null
			python dump2vec.py frequencies/Q1-Graph$graph-2-3-$graphlet$N.dump > frequencies/Q1-Graph$graph-2-3-$graphlet$N.csv
		done
	done
done


for graphlet in dir undir
do
	if [[ "${graphlet}" == "undir" ]]
	then
		graphlets=( 3 4 5 )
		flag=u
	fi
	if [[ "${graphlet}" == "dir" ]]
	then
		graphlets=( 3 4 )
		flag=d
	fi 
	for N in ${graphlets[@]}
	do
		python filtercsv.py CGCS-Template.csv 0,1 > frequencies/template-0-1-$graphlet$N.csv
		../gtrieScanner -s $N -m subgraphs $graphlet$N.str -g frequencies/template-0-1-$graphlet$N.csv -$flag -f csv -oc frequencies/template-0-1-$graphlet$N.dump > /dev/null
		python dump2vec.py frequencies/template-0-1-$graphlet$N.dump > frequencies/template-0-1-$graphlet$N.csv

		python filtercsv.py CGCS-Template.csv 2,3 > frequencies/template-2-3-$graphlet$N.csv
		../gtrieScanner -s $N -m subgraphs $graphlet$N.str -g frequencies/template-2-3-$graphlet$N.csv -$flag -f csv -oc frequencies/template-2-3-$graphlet$N.dump > /dev/null
		python dump2vec.py frequencies/template-2-3-$graphlet$N.dump > frequencies/template-2-3-$graphlet$N.csv
	done
done
