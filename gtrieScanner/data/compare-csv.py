'''
Compare two CSV files extracted form the same dump to test the merged version
'''
import sys

import pandas as pd


df1 = pd.read_csv(sys.argv[1], index_col='Vertex')
df2 = pd.read_csv(sys.argv[2], index_col='Vertex')

# normalize the dataframes
df1 = df1.sort_index()
df2 = df2.sort_index()

df2 = df2[df1.columns]  # order the columns

if all(df1==df2):
    print("The two CSV files are structurally identical")
else:
    print("The two CSV files are not identical")
