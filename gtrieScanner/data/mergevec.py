"Merge two or more CSV files for the same graph and motifs"

import sys

import pandas as pd

MERGED = None

for filename in sys.argv[1:]:
    df = pd.read_csv(filename, index_col='Vertex')
    if MERGED is None:
        MERGED = df
    else:
        MERGED = MERGED.add(df, fill_value=0).fillna(0)

MERGED.to_csv(sys.stdout)
