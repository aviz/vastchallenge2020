import sys
from collections import Counter


def filter_csv(filename, types):
    counter = Counter()
    with open(filename, "r") as infile:
        while True:
            line = infile.readline().rstrip('\n\r')
            if not line:
                break
            field = line.split(',')
            try:
                etype = int(field[1])
                if etype not in types:
                    continue
            except ValueError:
                pass
            edge = field[0]+","+field[2]
            counter.update([edge])
    for edge, count in counter.items():
        print(edge, count, sep=",")


if __name__ == "__main__":
    INDEX = None
    if len(sys.argv) < 3:
        print(f"syntax: {sys.argv[0]} <csv file> types",
              file=sys.stderr)
        sys.exit(1)
    TYPES = {int(t) for t in sys.argv[2].split(',')}
    filter_csv(sys.argv[1], TYPES)
