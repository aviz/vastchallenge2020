"Converts a dump file into a vector file"

from collections import defaultdict, Counter
import sys
# from pprint import pprint


def process_dump(filename, index=None):
    vertex_motif = defaultdict(list)
    motifs = set()
    with open(filename, 'r') as infile:
        while True:
            line = infile.readline().rstrip('\n\r')
            if not line:
                break
            field = line.split(' ')
            pattern = field[0][:-1]
            motifs.add(pattern)
            for vertex in field[1:]:
                vertex_motif[int(vertex)].append(pattern)
    if index is None:
        index = dict(zip(motifs, range(len(motifs))))
    else:
        diff = motifs.difference(index.keys())
        if diff:
            print(f"Missing motif in index: {diff}",
                  file=sys.stderr)
            for motif in diff:
                index[motif] = len(index)
        diff = set(index.keys()).difference(motifs)
        for motif in diff:
            del index[motif]
        for i, motif in enumerate(index):
            index[motif] = i
    vertex_vector = {}
    for vertex, motifs in vertex_motif.items():
        counter = Counter(motifs)
        vector = []
        for i in index:
            vector.append(counter[i])
        vertex_vector[vertex] = vector
    return (vertex_vector, index)


def load_index(filename):
    index = {}
    with open(filename, 'r') as infile:
        while True:
            line = infile.readline().rstrip('\n\r')
            if not line:
                break
            index[line] = len(index)
    return index


if __name__ == "__main__":
    INDEX = None
    ARGC = len(sys.argv)
    if ARGC < 2:
        print(f"syntax: {sys.argv[0]} <dump file> [<index file>]",
              file=sys.stderr)
        sys.exit(1)
    elif ARGC > 2:
        INDEX = load_index(sys.argv[2])
    vectors, index = process_dump(sys.argv[1], INDEX)
    # pprint(vectors)
    # pprint(index, sort_dicts=False)
    print("Vertex,", end='')
    print(*index.keys(), sep=',')  # assumes the index keys are sorted
    for vertex in vectors:         # assumes the keys are sorted as well
        vector = vectors[vertex]
        print(vertex, end=',')
        print(*vector, sep=',')
