start=`date +%s`

python filtercsv.py /home/vast/CGCS-GraphData.csv 0 > /var/bigvast/apister/bigGraph-0-undir4.csv
../gtrieScanner -s 4 -m subgraphs undir4.str -g /var/bigvast/apister/bigGraph-0-undir4.csv -u -f csv -oc /var/bigvast/apister/bigGraph-0-undir4.dump > /dev/null
python dump2vec.py /var/bigvast/apister/bigGraph-0-undir4.dump > /var/bigvast/apister/bigGraph-0-undir4.csv

end=`date +%s`
runtime=$((end-start))

echo $runtime > time_bigGraph_undir4_0.txt
