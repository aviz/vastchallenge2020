#!/bin/bash

#N=3

#if [ ! -f Q1-Graph1.csv ]; then
#    echo "Missing graph Q1-Graph1.csv" 1>&2
#    exit 1
#fi

#if [ ! -f dir$N.str ]; then
#    echo "Missing motif file dir$N.str" 1>&2
#    exit 1
#fi

# Extract type=0 edges
#python filtercsv.py Q1-Graph1.csv 0 > Q1-Graph1-0.csv

# Compute motifs matching for directed graph 0
#../gtrieScanner -s $N -m subgraphs dir$N.str -g Q1-Graph1-0.csv -d -f csv -oc Q1-Graph1-0.dump > /dev/null
#python dump2vec.py Q1-Graph1-0.dump > Q1-Graph1-0.csv


# Candidate graph loop
for graph in 1 2 3 4 5
do
    if [ ! -f Q1-Graph$graph.csv ]; then
        echo "Missing graph Q1-Graph$graph.csv" 1>&2
        exit 1
    fi
    # edge type loop
    for edgeType in 0 1 2 3 4 5 6
    do
	# sizes of undirected graphlets
        for n in 3 4 5
        do
            python filtercsv.py Q1-Graph$graph.csv $edgeType > edge_lists/Q1-Graph$graph-$edgeType-undir$n.csv
        done
	# sizes of directed graphlets
        for n in 3 4
        do
            python filtercsv.py Q1-Graph$graph.csv $edgeType > edge_lists/Q1-Graph$graph-$edgeType-dir$n.csv
        done
    done
done


# template graph loop
for edgeType in 0 1 2 3 4 5 6
do
    # sizes of undirected graphlets
    for n in 3 4 5
    do
        python filtercsv.py CGCS-Template.csv $edgeType > edge_lists/template-$edgeType-undir$n.csv
    done
    # sizes of directed graphlets
    for n in 3 4
    do
        python filtercsv.py CGCS-Template.csv $edgeType > edge_lists/template-$edgeType-dir$n.csv
    done
done






for graph in 1 2 3 4 5
do
	for graphlet in dir undir
	do
		if [[ "${graphlet}" == "undir" ]]
		then
			graphlets=( 3 4 5 )
			flag=u
		fi
		if [[ "${graphlet}" == "dir" ]]
		then
			graphlets=( 3 4 )
			flag=d
		fi 
		for N in ${graphlets[@]}
		do
			echo $N
			echo $graphlet$N
			python filtercsv.py Q1-Graph$graph.csv 0,1 > edge_lists/Q1-Graph$graph-0-1-$graphlet$N.csv

			python filtercsv.py Q1-Graph$graph.csv 2,3 > edge_lists/Q1-Graph$graph-2-3-$graphlet$N.csv
		done
	done
done


for graphlet in dir undir
do
	if [[ "${graphlet}" == "undir" ]]
	then
		graphlets=( 3 4 5 )
		flag=u
	fi
	if [[ "${graphlet}" == "dir" ]]
	then
		graphlets=( 3 4 )
		flag=d
	fi 
	for N in ${graphlets[@]}
	do
		python filtercsv.py CGCS-Template.csv 0,1 > edge_lists/template-0-1-$graphlet$N.csv
		
		python filtercsv.py CGCS-Template.csv 2,3 > edge_lists/template-2-3-$graphlet$N.csv
	done
done






