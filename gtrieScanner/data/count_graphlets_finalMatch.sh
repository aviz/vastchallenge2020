#!/bin/bash

#N=3

#if [ ! -f Q1-Graph1.csv ]; then
#    echo "Missing graph Q1-Graph1.csv" 1>&2
#    exit 1
#fi

#if [ ! -f dir$N.str ]; then
#    echo "Missing motif file dir$N.str" 1>&2
#    exit 1
#fi

# Extract type=0 edges
#python filtercsv.py Q1-Graph1.csv 0 > Q1-Graph1-0.csv

# Compute motifs matching for directed graph 0
#../gtrieScanner -s $N -m subgraphs dir$N.str -g Q1-Graph1-0.csv -d -f csv -oc Q1-Graph1-0.dump > /dev/null
#python dump2vec.py Q1-Graph1-0.dump > Q1-Graph1-0.csv

python filtercsv.py /home/apister/vastchallenge2020/data/final_matching/matched_nodes_with_demographics_filtered_with_node66_noIndexCol.csv 0 > finalMatch-0-undir5.csv
../gtrieScanner -s 5 -m subgraphs undir5.str -g finalMatch-0-undir5.csv -u -f csv -oc finalMatch-0-undir5.dump > /dev/null
python dump2vec.py finalMatch-0-undir5.dump > finalMatch-0-undir5.csv

python filtercsv.py /home/apister/vastchallenge2020/data/final_matching/matched_nodes_with_demographics_filtered_with_node66_noIndexCol.csv 1 > finalMatch-1-undir5.csv
../gtrieScanner -s 5 -m subgraphs undir5.str -g finalMatch-1-undir5.csv -u -f csv -oc finalMatch-1-undir5.dump > /dev/null
python dump2vec.py finalMatch-1-undir5.dump > finalMatch-1-undir5.csv

python filtercsv.py /home/apister/vastchallenge2020/data/final_matching/matched_nodes_with_demographics_filtered_with_node66_noIndexCol.csv 0,1 > finalMatch-0-1-undir5.csv
../gtrieScanner -s 5 -m subgraphs undir5.str -g finalMatch-0-1-undir5.csv -u -f csv -oc finalMatch-0-1-undir5.dump > /dev/null
python dump2vec.py finalMatch-0-1-undir5.dump > finalMatch-0-1-undir5.csv


# matched nodes intra
python filtercsv.py /home/apister/vastchallenge2020/data/final_matching/matched_nodes_intra_noIndexCol.csv 0 > finalMatchIntra-0-undir5.csv
../gtrieScanner -s 5 -m subgraphs undir5.str -g finalMatchIntra-0-undir5.csv -u -f csv -oc finalMatchIntra-0-undir5.dump > /dev/null
python dump2vec.py finalMatchIntra-0-undir5.dump > finalMatchIntra-0-undir5.csv

python filtercsv.py /home/apister/vastchallenge2020/data/final_matching/matched_nodes_intra_noIndexCol.csv 1 > finalMatchIntra-1-undir5.csv
../gtrieScanner -s 5 -m subgraphs undir5.str -g finalMatchIntra-1-undir5.csv -u -f csv -oc finalMatchIntra-1-undir5.dump > /dev/null
python dump2vec.py finalMatchIntra-1-undir5.dump > finalMatchIntra-1-undir5.csv

python filtercsv.py /home/apister/vastchallenge2020/data/final_matching/matched_nodes_intra_noIndexCol.csv 0,1 > finalMatchIntra-0-1-undir5.csv
../gtrieScanner -s 5 -m subgraphs undir5.str -g finalMatchIntra-0-1-undir5.csv -u -f csv -oc finalMatchIntra-0-1-undir5.dump > /dev/null
python dump2vec.py finalMatchIntra-0-1-undir5.dump > finalMatchIntra-0-1-undir5.csv
