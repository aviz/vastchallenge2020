import os.path
import random
from collections import defaultdict

import pandas as pd
import numpy as np
import pyarrow.parquet as pq
import networkx as nx

def df_neighbors(G_df, node):
    if node in G_df.index:
        neighbors = G_df.loc[node]['Target']
        if type(neighbors) == np.int64:
            return [neighbors]
        else:
            return neighbors
    else:
        return []


def extract_people_node(G_df, G_big_df):
    G_people = G_df[G_df['eType'].isin([0,1])]
    people = set(G_people.index).union(set(G_people['Target']))
    #print(people)

    equal_freqs = defaultdict(list)
    for person in people:
        if person in G_df.index:
            candidate_neighbors = G_df.loc[person]
            big_graph_neighbors = G_big_df.loc[person]

            for edge_type in [2,3,4,5,6]:
                #print('edge type', edge_type)
                neighbors = candidate_neighbors[candidate_neighbors['eType'] == edge_type]['Target']
                neighbors_big_graph = big_graph_neighbors[big_graph_neighbors['eType'] == edge_type]['Target']
                #print(set(neighbors) == set(neighbors_big_graph))
                equal_freqs[edge_type].append(set(neighbors) == set(neighbors_big_graph))
                #print(set(neighbors), len(set(neighbors)))
                #print(set(neighbors_big_graph), len(set(neighbors_big_graph))

    #print(equal_freqs)
    print({k:sum(v) / len(v) for k, v in equal_freqs.items()})


table = pq.read_table('/home/vast/CGCS-GraphData.parquet', columns=['Source', 'Target', 'eType'])
G_big_df = table.to_pandas()
#df = df.reset_index()
#df = df.drop_duplicates()

#print(df.head)
#print(df.columns)

#print('ok')


G1_df = pd.read_csv("/home/vast/MC1/data/Q1-Graph1.csv", usecols=['Source', 'Target', 'eType'], index_col="Source")
G2_df = pd.read_csv("/home/vast/MC1/data/Q1-Graph2.csv", usecols=['Source', 'Target', 'eType'], index_col="Source")
G3_df = pd.read_csv("/home/vast/MC1/data/Q1-Graph3.csv", usecols=['Source', 'Target', 'eType'], index_col="Source")
G4_df = pd.read_csv("/home/vast/MC1/data/Q1-Graph4.csv", usecols=['Source', 'Target', 'eType'], index_col="Source")
G5_df = pd.read_csv("/home/vast/MC1/data/Q1-Graph5.csv", usecols=['Source', 'Target', 'eType'], index_col="Source")


extract_people_node(G1_df, G_big_df)
extract_people_node(G2_df, G_big_df)
extract_people_node(G3_df, G_big_df)
extract_people_node(G4_df, G_big_df)
extract_people_node(G5_df, G_big_df)

