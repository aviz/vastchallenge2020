import os.path

import sys

import numpy as np

import dask.dataframe as dd
import pyarrow.parquet as pq


def create_parquet(infilename, outfilename):
    '''Converts the large contest dataset in csv 
    format into a "parquet" file very fast to load
    and split in multiple partitions (subfiles).
    Also create multiple index files to access
    relevant data quickly.
    '''
    if os.path.exists(outfilename):
        return  # skip if file exists already
    df = dd.read_csv(infilename,
                     dtype={
                         'Source': np.int32,
                         'eType': np.int32,
                         'Target': np.int32,
                         'Time': np.int64,
                         'Weight': np.float32,
                         'SourceLocation': np.float32,
                         'TargetLocation': np.float32,
                         'SourceLatitude': np.float32,
                         'SourceLongitude': np.float32,
                         'TargetLatitude': np.float32,
                         'TargetLongitude': np.float32
                     })
    # Note that the locations are either a number 1-5
    # or not specified, which becomes NaN in python.
    # but using a float is expensive so insted, convert
    # the two Location columns to int32 by replacing
    # the NaN with -1
    SourceLocation = df.SourceLocation.fillna(-1)  # NaN <- -1
    TargetLocation = df.TargetLocation.fillna(-1)  # NaN <- -1

    # Now, we only have integers so convert the column types
    SourceLocation = SourceLocation.astype(dtype=np.int32)
    TargetLocation = TargetLocation.astype(dtype=np.int32)
    # And replace the old float32 columns by our new int32 ones
    df.SourceLocation = SourceLocation
    df.TargetLocation = TargetLocation
    # Set the index to the 'Source' node id so the data is
    # indexed by node id
    df = df.set_index('Source')
    # Write the results to a parquet file.
    df.to_parquet(outfilename, engine='pyarrow')


def create_sorted_index(filename, column):
    "Create a sorted index for the specified column"
    outfilename = column+'_idx.npy'
    print('Computing the index for', column)
    if os.path.exists(outfilename):
        print('Already done.')
        return

    source = pq.read_table(filename,
                           columns=[column]).to_pandas()
    # One column fits in memory
    if column == source.index.name:
        values = source.index.values.reshape(-1)  # flatten
        (unique, counts) = np.unique(
            source.index.values,
            return_counts=True)
    else:
        values = source.values.reshape(-1)  # flatten
        (unique, counts) = np.unique(
            source.values,
            return_counts=True)
    order = np.argsort(values, kind='stable')
    order = order.astype(dtype=np.int32, copy=False)
    unique = unique.astype(np.int32, copy=False)
    counts = counts.astype(np.int32, copy=False)
    start = np.searchsorted(values, unique, sorter=order)
    start = start.astype(np.int32, copy=False)
    with open(outfilename, 'w') as out:
        order.tofile(out)
    with open(column+'_unique.npy', 'w') as out:
        unique.tofile(out)
    with open(column+'_counts.npy', 'w') as out:
        counts.tofile(out)
    with open(column+'_start.npy', 'w') as out:
        start.tofile(out)
    print('Done.')


if __name__ == "__main__":
    INFILE = 'CGCS-GraphData.csv'
    OUTFILE = 'CGCS-GraphData.parquet'
    if len(sys.argv) > 1:
        INFILE = sys.argv[1]
    if len(sys.argv) > 2:
        OUTFILE = sys.argv[2]
    # from dask.distributed import Client

    # CLIENT = Client(processes=False)
    # print("URL is:", CLIENT.dashboard_link)
    create_parquet(INFILE, OUTFILE)
    create_sorted_index(OUTFILE, 'Source')
    create_sorted_index(OUTFILE, 'Target')
