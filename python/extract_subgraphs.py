import os.path
import random

import pandas as pd
import numpy as np
import pyarrow.parquet as pq
import networkx as nx

table = pq.read_table('/home/vast/CGCS-GraphData.parquet', columns=['Source', 'Target', 'eType'])
df = table.to_pandas()
df = df.reset_index()
df = df.drop_duplicates()
df = df.set_index('Source')

df_coms = df[df['eType'].isin([0,1])]

print('df coms')
print(df_coms.head)
#print(df.columns)

#print('ok')

def df_neighbors(G_df, node):
    if node in G_df.index:
        neighbors = G_df.loc[node]['Target']
        if type(neighbors) == np.int64:
            return [neighbors]
        else:
            return neighbors
    else:
        return []
    

def df_bfs(G_df, node, p_eviction=0.01, threshold=80, neighbors_max=30):
    visited = [node]
    queue = [node]
    evicted_nodes = []
    
    first_round = True
    i = 0
    while len(visited) < threshold and queue:
        n = queue.pop()
        neighbors = df_neighbors(G_df, n)
        
        neighbors = list(set(neighbors) - set(evicted_nodes))
        
        # if the neighborhhod is too big, take only a sample of it
        if len(neighbors) > neighbors_max:
            neighbors = random.sample(neighbors, neighbors_max)

        N_neighbors = len(neighbors)
        
        if not(first_round):
            r = random.random()
            print(p_eviction * N_neighbors)
            if  r < p_eviction * N_neighbors:
                evicted_nodes.append(n)
                continue
        
        for neighbor in neighbors:
            if neighbor not in visited and neighbor not in queue:
                visited.append(neighbor)
                queue.append(neighbor)
        
        first_round = False
        i += 1
   
    print('number of rounds : ', i)

    #print(visited)
    #print(len(visited))
    return visited

def random_walk(G_df, node_start, degree_threshold=1000, size=90):
    dict_neighbors = {}
    visited = {node_start}
    current_node = node_start
    while len(visited) < size:
        if current_node in dict_neighbors.keys():
            neighbors = dict_neighbors[current_node]
        else:
            neighbors = df_neighbors(G_df, current_node)
            dict_neighbors[current_node] = neighbors

        if len(neighbors) > degree_threshold:
            current_node = random.sample(visited, 1)[0]
            continue

        current_node = random.sample(set(neighbors), 1)[0]
        visited.add(current_node)
    print('random walk finished')
    return visited


def random_walk_2(G_df, node_start, size=90):
    pass
    


def induced_subgraph(G_df, nodes):
    '''
    From a list of nodes returned the induced subgraph in dataframe format
    '''
    nodes_source = [node for node in nodes if node in G_df.index]
    source_filter = G_df.loc[nodes_source, :]
    subgraph_df = source_filter[source_filter['Target'].isin(nodes)]
    return subgraph_df


def random_node(G_df):
    '''select random node'''
    return G_df.sample().index[0]
    
    
#subgraph = df_bfs(df, 533140, threshold=80)
#print(subgraph)

#print(induced_subgraph(df, subgraph))





N = 100
size = 80

def generate_subgraphs_bfs(df, N=100, size=80):
    for n in range(N):
        print(n)
        start_node = random_node(df)
        print('start node : ', start_node)
        graph_nodes = df_bfs(df, start_node, p_eviction=0.01, threshold=size)
        print('size : ', len(graph_nodes))
        subgraph = induced_subgraph(df, graph_nodes)
        subgraph.to_csv('extracted_graphs/' + 'graph' + str(n) + '.csv')


def generate_subgraphs_random_walk(G_df, N, size_interval=[80,100], deg_thresh=10000):
    for n in range(N):
        print(n)
        start_node = random_node(df)
        print('start node : ', start_node)
        size = random.randint(size_interval[0], size_interval[1])
        graph_nodes = random_walk(G_df, start_node, degree_threshold=deg_thresh, size=size)
        print('size : ', len(graph_nodes))
        subgraph = induced_subgraph(df, graph_nodes)
        subgraph.to_csv('extracted_graphs/random_walk_nodegthresh/' + 'graph' + str(n) + '.csv')

generate_subgraphs_random_walk(df_coms, N=100, size_interval=[10, 40], deg_thresh=100000000000)
