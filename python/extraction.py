#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os.path
import random

import pandas as pd
import numpy as np
import pyarrow.parquet as pq

#table = pq.read_table('/home/vast/CGCS-GraphData.parquet', columns=['Source', 'Target'])
#df = table.to_pandas()
#
#print(df.head)


#%%

path = '../data/Q1-Graph1.csv'
df = pd.read_csv(path, usecols=['Source', 'Target', 'eType'])
df = df.set_index('Source')


def df_neighbors(G_df, node):
    if node in G_df.index:
        neighbors = G_df.loc[node]['Target']
        print(neighbors)
        return G_df.loc[node]['Target']
    else:
        return []
    

def df_bfs(G_df, node, p_eviction=0.01, threshold=80):
    visited = [node]
    queue = [node]
    evicted_nodes = []
    
    first_round = True
    while len(visited) < threshold and queue:
        n = queue.pop()
        neighbors = df_neighbors(G_df, n)
        
        neighbors = list(set(neighbors) - set(evicted_nodes))
        N_neighbors = len(neighbors)
        
        if not(first_round):
            r = random.random()
            print(p_eviction * N_neighbors)
            if  r < p_eviction * N_neighbors:
                evicted_nodes.append(n)
                continue
        
        for neighbor in neighbors:
            if neighbor not in visited and neighbor not in queue:
                visited.append(neighbor)
                queue.append(neighbor)
        
        first_round = False
#    print(visited)
        
        
def induced_subgraph(G_df, nodes):
    '''
    From a list of nodes returned the induced subgraph in dataframe format
    '''
    source_filter = G_df.loc[nodes, :]
    subgraph_df = source_filter[source_filter['Target'].isin(nodes)]
    return subgraph_df
    
    
    
df_bfs(df, 616050, threshold=80)


    
    
