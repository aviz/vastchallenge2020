import os.path

import pandas as pd
import numpy as np
import pyarrow.parquet as pq
import pyarrow

# This has to be big enough you don't get a chunk of all nulls: https://issues.apache.org/jira/browse/ARROW-2659
SPLIT_ROWS = 2 ** 17

def create_parquet(infilename, outfilename):
    if os.path.exists(outfilename):
        return
    writer = None
    for split in pd.read_csv(infilename,
                             chunksize=SPLIT_ROWS,
                             index_col=False,
                             dtype={
                                 'Source': np.int32,
                                 'eType': np.int32,
                                 'Target': np.int32,
                                 'Time': np.int64,
                                 'Weight': np.float32,
                                 'SourceLocation': np.float32,
                                 'TargetLocation': np.float32,
                                 'SourceLatitude': np.float32,
                                 'SourceLongitude': np.float32,
                                 'TargetLatitude': np.float32,
                                 'TargetLongitude': np.float32
                             },
                             memory_map=True):
        # print(split.head())
        # import pdb; pdb.set_trace()
        split.SourceLocation.fillna(-1, inplace=True)
        split.TargetLocation.fillna(-1, inplace=True)
        split.SourceLocation = split.SourceLocation.astype(np.int32)
        split.TargetLocation = split.TargetLocation.astype(np.int32)
        table = pyarrow.Table.from_pandas(split, preserve_index=False)
        # Timestamps have issues if you don't convert to ms. https://github.com/dask/fastparquet/issues/82
        writer = writer or pq.ParquetWriter(
            outfilename,
            table.schema)
        writer.write_table(table)
    writer.close()

if __name__ == "__main__":
    create_parquet('CGCS-GraphData.csv', 'CGCS-GraphData.parquet')
