import os.path
import random
import sys


import pandas as pd
import numpy as np
import pyarrow.parquet as pq
import networkx as nx
import altair as alt
from IPython.display import display
import matplotlib.pyplot as plt
import scipy as sp
from scipy.sparse import coo_matrix, csr_matrix, csc_matrix


matrix_dir = '../notebooks/'
matrix_all_fp = matrix_dir + 'communication_matrix_all.npz'
index_all_fp = matrix_dir + 'communication_unique_all.npy'

table = pq.read_table('/home/vast/CGCS-GraphData.parquet')
df_complete = table.to_pandas().reset_index()

G_all = sp.sparse.load_npz(matrix_all_fp)
G_all = csr_matrix(G_all)

G_all_edges = G_all.nonzero()
G_all_edges = np.array(G_all_edges)

indexes_all = np.fromfile(index_all_fp, dtype=np.int64)

seed1 = (600971, 579269) #4
seed2 = (538771, 473043) #4
seed3 = (574136, 657187) #2

seed1_ind = np.where(indexes_all == seed1[0])[0][0]
seed2_ind = np.where(indexes_all == seed2[0])[0][0]
seed3_ind = np.where(indexes_all == seed3[0])[0][0]

def induced_subgraph(all_edges, nodes):
    source_filter = all_edges[: ,np.isin(all_edges[0], list(nodes))]
    target_filter = source_filter[:, np.isin(source_filter[1], list(nodes))]
    
    return target_filter

def np_edges_to_tuples(edges):
    edges_tuples = {(u,v) for u,v in zip(edges[0], edges[1])}
    return edges_tuples

def neighbors_out(G, node):
    return G[node].nonzero()[1]

def neighbors_in(G, node):
    return G[:,node].nonzero()[0]

def neighbors_all(G, node):
    neighbors = set(neighbors_in(G, node)).union(set(neighbors_out(G, node)))
    return np.array(list(neighbors))

def random_walk_simple(G, node_start, n_nodes):
    visited = {node_start}
    visited_edges = set()
    
    current_node = node_start
    
    i = 0
    while len(visited) < n_nodes:
#         print(i)  
        neighbors = neighbors_all(G, current_node)
    
        if set(neighbors).issubset(visited):
            print('all neighbors are already visited')
            current_node = random.sample(visited, 1)[0]
            continue
        
        
        # choose which target to choose given their degree
        target_node = np.random.choice(list(neighbors), 1)[0]
        visited.add(target_node)
        
        
        current_node = target_node
        i += 1
        
    print('random walk finished')
    return visited


import time
def generate_graph_seed_pop_all_channels(G, G_edges, indexes, start, N, size_interval, seed_index, path):
    for n in range(start, N):
        print(n)
        start_time = time.time()
        size = random.randint(size_interval[0], size_interval[1])
        
        nodes = random_walk_simple(G, node_start=seed_index, n_nodes=size)
        induced_graph = induced_subgraph(G_edges, nodes)
        edge_list = np_edges_to_tuples(induced_graph)
        df_edge_list = pd.DataFrame({'Source': [indexes[edge[0]] for edge in edge_list], 'Target': [indexes[edge[1]] for edge in edge_list]})

        
        
        print('merge the extracted graph with big graph dataframe')
        # Retrieve the edges in the big graph DF 
        subgraph_final = pd.merge(df_edge_list, df_complete,  how='left', on=['Source', 'Target'])

        
        subgraph_final.to_csv(path + 'graph' + str(n) + '.csv')
        
#         G_nx = nx.from_pandas_edgelist(subgraph_df, source='Source', target='Target', create_using=nx.DiGraph)
        
#         if n < 60:
#             fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(12, 8))
#             nx.draw_networkx(G_nx, pos=nx.kamada_kawai_layout(G_nx), ax=axes, font_color='red')
#             plt.show()

        print("--- %s seconds ---" % (time.time() - start_time))



if __name__ == '__main__':
    for i, arg in enumerate(sys.argv):
        if i == 1:
            if arg not in ['1','2','3']:
                print('seed is 1,2 or 3')
            seed = arg
            break

    if seed == '1':
        seed_ind = seed1_ind
    elif seed == '2':
        seed_ind = seed2_ind
    elif seed == '3':
        seed_ind = seed3_ind
    else:
        print('error in argument')

    print('generating graphs for seed ' + arg)
    generate_graph_seed_pop_all_channels(G_all, G_all_edges, indexes_all, 0, 50, [88, 88], seed_ind, path='/home/apister/vastchallenge2020/python/extracted_graphs_seed/RW_all_channels/seed' + seed)
